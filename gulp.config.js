module.exports = function() {
    var config = {
        temp: './.tmp/',
        index: './index.html',

        alljs: [
            './app/**/*.js',
            './_modules/**/*.js',
            './*.js'
        ],

        allsass: [
            './sass/*.scss'
        ],

        appscripts: [
            './app/app.js',
            './app/config.js',
            './app/run.js',
            './app/constants.js',
            './app/filters.js',
            './app/routes.js',
            './app/**/*.js',
            './_modules/**/*.js'
        ],

        css: './css/',

        sass: [
            './sass/styles.scss'
        ],

        vendorscripts: [
            './js/lodash/lodash.js',
            './js/moment/moment.js',
            './js/jquery/jquery.js',
            './js/api-check/api-check.js',
            './js/bootstrap/bootstrap.js',
            './js/angular/angular.js',
            './js/angular-ui-router/angular-ui-router.js',
            './js/angular-bootstrap/ui-bootstrap-tpls.js',
            './js/angular-resource/angular-resource.js',
            './js/angular-messages/angular-messages.js',
            './js/angular-animate/angular-animate.js',
            './js/angular-sanitize/angular-sanitize.js',
            './js/satellizer/satellizer.js',
            './js/angular-toastr/angular-toastr.tpls.js',
            './js/angular-ui-mask/mask.js',
            './js/angular-loading-bar/loading-bar.js',
            './js/angular-gravatar/angular-gravatar.js',
            './js/angular-formly/formly.js',
            './js/angular-formly-templates-bootstrap/angular-formly-templates-bootstrap.js',
            './js/ng-focus-on/ng-focus-on.js'
        ]
    };
    return config;
};
