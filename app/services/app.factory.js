(function() {
    'use strict';
    angular
        .module('ECO')
        .factory('AppFactory', AppFactory);

    AppFactory.$inject = ['$rootScope', '$auth', '$http', '$state', 'API_URL'];

    /* @ngInject */
    function AppFactory($rootScope, $auth, $http, $state, API_URL) {
        var publicAPI = {
            logout: logout
        };
        return publicAPI;

        //////////
        function logout() {
            $auth.logout()
                .then(function() {
                    localStorage.removeItem('user');
                    $rootScope.authenticated = false;
                    $rootScope.currentUser = null;
                    $state.go('auth', {});
                });
        }
    } // end factory
})();