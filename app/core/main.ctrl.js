(function() {
    'use strict';
    angular
        .module('ECO')
        .controller('MainController', MainController);

        MainController.$inject = ['$rootScope', 'AppFactory'];

        /* @ngInject */
        function MainController($rootScope, AppFactory) {
            /* jshint validthis: true */
            var vm = this;

            //BUILD ONLY
            var user = {
                firstname: 'Jonathan',
                lastname: 'Gravois',
                email: 'jongravois@gmail.com'
            };
            if( ! $rootScope.currentUser) {
                $rootScope.currentUser = user;
            }
            //BUILD ONLY

            //vm.authenticated = $rootScope.authenticated;
            vm.current_year = new Date().getFullYear();
            vm.AppFactory = AppFactory;

            vm.status = {
                isopen: false
            };

            vm.toggled = function(open) {};

            vm.toggleDropdown = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                vm.status.isopen = !vm.status.isopen;
            };
            //////////

        } // end function
})();
