(function() {
    'use strict';
    angular
        .module('ECO')
        .run(function ($rootScope, $location) {
            $rootScope.$on('$stateChangeSuccess', function(event, toState) {
                $rootScope.path = toState.url.substr(1);
                //console.log($rootScope.path);
            });
        })
        .run(function ($rootScope, $location, $anchorScroll) {
            $rootScope.$on('$stateChangeSuccess', function() {
                document.body.scrollTop = document.documentElement.scrollTop = 0;
            });
        });
    /*.run(function($window) {
     $window.onbeforeunload = function($window) {
     if ($scope.dirty) {
     return "The form is dirty, do you want to stay on the page?";
     }
     return 'You have requested a browser refresh. Any unsaved or unconfirmed changes will be lost.';
     };
     })*/
})();
