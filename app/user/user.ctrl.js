(function () {
    'use strict';
    angular
        .module('ECO')
        .controller('UserController', UserController);

    UserController.$inject = ['$rootScope', '$auth', '$http', '$state', 'AppFactory', 'API_URL'];

    /* @ngInject */
    function UserController($rootScope, $auth, $http, $state, AppFactory, API_URL) {
        /* jshint validthis: true */
        var vm = this;
        vm.users = [];
        vm.error = [];

        vm.getUsers = function () {
            $http.get(API_URL + 'authenticate')
                .success(function (users) {
                    vm.users = users;
                })
                .error(function (error) {
                    vm.error = error;
                });
        };
        vm.logout = AppFactory.logout;
    } // end controller
})();
