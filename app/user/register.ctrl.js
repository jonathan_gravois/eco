(function() {
    'use strict';
    angular
        .module('ECO')
        .controller('RegisterController', RegisterController);

        RegisterController.$inject = ['$state', 'FeederFactory'];

        /* @ngInject */
        function RegisterController($state, FeederFactory) {
            /* jshint validthis: true */
            var vm = this;

            FeederFactory
                .getCountries()
                .then(function(rsp) {
                    vm.countries = rsp.data;
                });

            vm.newuser = {
                individual: false
            };

            vm.companyusers = [
                {
                    firstname: 'Tony',
                    lastname: 'Stark',
                    title: 'CEO',
                    email: 'ironman@marvel.com'
                },
                {
                    firstname: 'Bruce',
                    lastname: 'Wayne',
                    title: 'CFO',
                    email: 'batman@marvel.com'
                },
                {
                    firstname: 'Natasha',
                    lastname: 'Romanoff',
                    title: 'COO',
                    email: 'blackwidow@marvel.com'
                }
            ];

            vm.addUser = function() {
                vm.companyusers.push(vm.newb);
                vm.newb = {};
            };

            vm.registerCompany = function() {
                $state.go('register2', {});
            };

            vm.registerUsers = function() {
                alert('Form Saved');
            };

            //////////

        } // end controller
})();