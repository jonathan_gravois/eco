(function() {
    'use strict';
    angular
        .module('ECO')
        .controller('AuthController', AuthController);

        AuthController.$inject = ['$rootScope', '$auth', '$state', '$http', 'API_URL'];

        /* @ngInject */
        function AuthController($rootScope, $auth, $state, $http, API_URL) {
            /* jshint validthis: true */
            var vm = this;

            vm.loginError = false;
            vm.loginErrorText;

            vm.login = function() {
                var credentials = {
                    email: vm.email,
                    password: vm.password
                };

                $auth
                    .login(credentials)
                    .then(function() {
                        return $http.get(API_URL + 'authenticate/user');
                    }, function(error) {
                        vm.loginError = true;
                        vm.loginErrorText = error.data.error;
                    })
                    .then(function(response) {
                        var resp = response.data.user;
                        var user = JSON.stringify(resp);
                        localStorage.setItem('user', user);
                        $rootScope.authenticated = true;
                        $rootScope.currentUser = resp;
                        $state.go('eco.home', {});
                    });
            };
        } // end function
})();
