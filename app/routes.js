(function() {
    'use strict';
    angular
        .module('ECO')
        .config(function($stateProvider, $urlRouterProvider, $authProvider, API_URL) {
            $urlRouterProvider.otherwise('/auth');
            $stateProvider
                .state('auth', {
                    url: '/auth',
                    templateUrl: './app/user/auth.view.html',
                    controller: 'AuthController as auth'
                })
                .state('forgotpw', {
                    url: '/forgotpw',
                    templateUrl: './app/user/forgotpw.view.html',
                    controller: 'RemindController as forgot'
                })
                .state('register', {
                    url: '/register',
                    templateUrl: 'app/user/register.view.html',
                    controller: 'RegisterController as reg'
                })
                .state('register2', {
                    url: '/register2',
                    templateUrl: 'app/user/register2.view.html',
                    controller: 'RegisterController as reg'
                })
                .state('eco', {
                    url: '/eco',
                    abstract: true,
                    templateUrl: 'app/layouts/logged.view.html',
                    controller: 'UserController as user'
                })
                .state('eco.home', {
                    url: '/home',
                    templateUrl: 'app/projects/dash.tmpl.html',
                    controller: 'DashboardController as dash'
                })
                .state('eco.prefs', {
                    url: '/prefs',
                    templateUrl: 'app/user/prefs.tmpl.html',
                    controller: 'SettingsController as prefs'
                })
                .state('eco.users', {
                    url: '/users',
                    templateUrl: './app/user/users.view.html',
                    controller: 'UserController as user'
                })
                .state('eco.project', {
                    url: '/project',
                    abstract: true,
                    templateUrl: 'app/projects/new.view.html',
                    controller: 'ProjectsController as vm'
                })
                .state('eco.project.new', {
                    url: '/new',
                    templateUrl: 'app/projects/new.tmpl.html'
                })
                .state('eco.project.aircraft', {
                    url: '/aircraft',
                    templateUrl: 'app/projects/aircraft.tmpl.html'
                })
                .state('eco.project.engines', {
                    url: '/engines',
                    templateUrl: 'app/projects/engines.tmpl.html'
                })
                .state('eco.project.parts', {
                    url: '/parts',
                    templateUrl: 'app/projects/parts.tmpl.html'
                })
                .state('eco.project.summary', {
                    url: '/summary',
                    templateUrl: 'app/projects/summary.tmpl.html'
                });
        });
})();
