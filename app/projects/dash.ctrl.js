(function() {
    'use strict';
    angular
        .module('ECO')
        .controller('DashboardController', DashboardController);

        DashboardController.$inject = ['$state'];

        /* @ngInject */
        function DashboardController($state) {
            /* jshint validthis: true */
            var vm = this;
            vm.path = $state.current.name;
            //////////

        } // end controller
})();