(function() {
    'use strict';
    angular
        .module('ECO')
        .controller('ProjectsController', ProjectsController);

        ProjectsController.$inject = ['$state', 'FeederFactory'];

        /* @ngInject */
        function ProjectsController($state, FeederFactory) {
            /* jshint validthis: true */
            var vm = this;
            vm.pbar_start = false;

            FeederFactory
                .getMocks()
                .then(function(rsp) {
                    vm.mocks = rsp.data;
                });
            FeederFactory
                .getLocations()
                .then(function(rsp) {
                    var raw = rsp.data;
                    vm.locations = raw;
                    //console.log('Locations', raw);

                    //TODO: Remove Corporate from above
                    //vm.techlocations = techs;

                    vm.techlocations = [
                        {
                            id: 2,
                            location: "Mobile Disassembly"
                        },
                        {
                            id: 3,
                            location: 'Tupelo, Mississippi, USA'
                        }
                    ];
                });

            vm.new_pbar = {
                aircraft: false,
                engines: false,
                parts: false,
                summary: false
            };

            vm.moveToAircraft = function() {
                vm.new_pbar.aircraft = true;
                vm.pbar_start = true;
                $state.go('eco.project.aircraft', {});
            };
            vm.moveToEngines = function() {
                vm.new_pbar.engines = true;
                $state.go('eco.project.engines', {});
            };
            vm.moveToParts = function() {
                vm.new_pbar.parts = true;
                $state.go('eco.project.parts', {});
            };
            vm.moveToSummary = function() {
                vm.new_pbar.summary = true;
                $state.go('eco.project.summary', {});
            };
            vm.submitNewProject = function() {
                alert('Submitting Project for Processing and Approval');
            };
            //////////
            vm.newb = {
                project_type: "1",
            };

        } // end controller
})();