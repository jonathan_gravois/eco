(function() {
    'use strict';
    angular
        .module('ECO')
        .factory('FeederFactory', FeederFactory);

    FeederFactory.$inject = ['$http', 'API_URL'];

    /* @ngInject */
    function FeederFactory($http, API_URL) {
        var publicAPI = {
            getCompanies: getCompanies,
            getCountries: getCountries,
            getLocations: getLocations,
            getMocks: getMocks
        };
        return publicAPI;

        //////////
        function getCompanies() {
            return $http.get(API_URL + 'companies');
        }
        function getCountries() {
            return $http.get(API_URL + 'countries');
        }
        function getLocations() {
            return $http.get(API_URL + 'locations');
        }
        function getMocks() {
            return $http.get(API_URL + 'mocks');
        }
    } // end factory
})();