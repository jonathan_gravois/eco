(function() {
    'use strict';
    angular
        .module('ECO', [
            'ui.router',
            'ui.bootstrap',
            'satellizer',
            'toastr',
            'ui.mask',
            'angular-loading-bar',
            'ui.gravatar',
            'formly',
            'formlyBootstrap'
        ])
        .run(function ($rootScope, $location) {
            $rootScope.$on('$stateChangeSuccess', function(event, toState) {
                $rootScope.path = toState.url.substr(1);
                //console.log($rootScope.path);
            });
        });
})();
