(function() {
    'use strict';
    angular
        .module('ECO', [
            'ui.router',
            'ui.bootstrap',
            'satellizer',
            'toastr',
            'ui.mask',
            'angular-loading-bar',
            'ui.gravatar',
            'formly',
            'formlyBootstrap'
        ])
        .run(function ($rootScope, $location) {
            $rootScope.$on('$stateChangeSuccess', function(event, toState) {
                $rootScope.path = toState.url.substr(1);
                //console.log($rootScope.path);
            });
        });
})();

(function() {
    'use strict';
    angular
        .module('ECO')
        .config(function(toastrConfig) {
            angular.extend(toastrConfig, {
                closeButton: true,
                timeOut: 3000
            });
        })
        .config(function() {
            function findByValues(collection, property, values) {
                return _.filter(collection, function(item) {
                    return _.contains(values, item[property]);
                });
            }

            function groupByMulti(obj, values, context) {
                if (!values.length) {
                    return obj;
                }
                var byFirst = _.groupBy(obj, values[0], context),
                    rest = values.slice(1);
                for(var prop in byFirst) {
                    byFirst[prop] = _.groupByMulti(byFirst[prop], rest, context);
                }
                return byFirst;
            }

            function sumCollection(arr, val) {
                return _.reduce(arr, function(sum, item) {
                    return sum += Number(item[val]);
                }, 0);
            }

            function pluckuniq(col, val) {
                return _.first(_.uniq(_.pluck(col, val)));
            }

            /*
             * col (collection) | val (to be adjusted) | factor (percent field)
             */
            function weighted(col, val, factor) {
                var totalFactor = _.sumCollection(col, factor);

                return _.reduce(col, function(sum, current) {
                    return sum += (current[val]) * (current[factor] / totalFactor);
                }, 0);
            }

            function average(col, cb) {
                return _(col)
                        .map(cb)
                        .reduce(function(result, item) {
                            return result + item;
                        }) / _.size(col);
            }

            _.mixin({
                findByValues: findByValues,
                groupByMulti: groupByMulti,
                sumCollection: sumCollection,
                pluckuniq: pluckuniq,
                weighted: weighted,
                average: average
            });
        })
        .config(function($stateProvider, $urlRouterProvider, $authProvider, $httpProvider, $provide, API_URL) {
            function redirectWhenLoggedOut($q, $injector) {
                return {
                    responseError: function(rejection) {
                        var $state = $injector.get('$state');
                        var rejectionReasons = ['token_not_provided', 'token_expired', 'token_absent', 'token_invalid'];
                        angular.forEach(rejectionReasons, function(value, key) {

                            if(rejection.data && rejection.data.error === value) {
                                localStorage.removeItem('user');
                                $state.go('auth');
                            }
                        });

                        return $q.reject(rejection);
                    }
                };
            } // end redirect function

            $provide.factory('redirectWhenLoggedOut', redirectWhenLoggedOut);
            $httpProvider.interceptors.push('redirectWhenLoggedOut');

            $authProvider.loginUrl = API_URL + 'authenticate';
        });
})();

(function() {
    'use strict';
    angular
        .module('ECO')
        .run(function ($rootScope, $location) {
            $rootScope.$on('$stateChangeSuccess', function(event, toState) {
                $rootScope.path = toState.url.substr(1);
                //console.log($rootScope.path);
            });
        })
        .run(function ($rootScope, $location, $anchorScroll) {
            $rootScope.$on('$stateChangeSuccess', function() {
                document.body.scrollTop = document.documentElement.scrollTop = 0;
            });
        });
    /*.run(function($window) {
     $window.onbeforeunload = function($window) {
     if ($scope.dirty) {
     return "The form is dirty, do you want to stay on the page?";
     }
     return 'You have requested a browser refresh. Any unsaved or unconfirmed changes will be lost.';
     };
     })*/
})();

(function () {
    'use strict';
    angular
        .module('ECO')
        .constant('_', window._)
        .constant('CLIENT_URL', 'http://www.eco.local/')
        .constant('APP_URL', 'http://www.aero.local/')
        .constant('API_URL', 'http://www.aero.local/api/')
        .constant('LEGAL_NAME', 'AeroEco');
})();

(function () {
    angular.module('ECO')
        .filter('asDate', asDateFilter)
        .filter('booleanYN', booleanYNFilter)
        .filter('booleanYesNo', booleanYesNoFilter)
        .filter('dateFmt', dateFmtFilter)
        .filter('capitalize', capitalizeFilter)
        .filter('capitalizeFirst', capitalizeFirstFilter)
        .filter('displayname', displaynameFilter)
        .filter('displaynull', displaynullFilter)
        .filter('displaynNA', displayNAFilter)
        .filter('displaynullcurrency', displaynullcurrencyFilter)
        .filter('displaynullpercent', displaynullpercentFilter)
        .filter('displaynullsingle', displaynullsingleFilter)
        .filter('displaypercent', displaypercentFilter)
        .filter('boolean', booleanFilter)
        .filter('noCentsCurrency', noCentsCurrencyFilter)
        .filter('phone', phoneFilter)
        .filter('ssnum', ssnumFilter)
        .filter('stateabr', stateAbrFilter)
        .filter('justtext', justtextFilter)
        .filter('flexNumber', flexNumberFilter)
        .filter('flexZeroNumber', flexZeroNumberFilter)
        .filter('flexCurrency', flexCurrencyFilter)
        .filter('flexZeroCurrency', flexZeroCurrencyFilter)
        .filter('flexNACurrency', flexNACurrencyFilter)
        .filter('flexPercent', flexPercentFilter)
        .filter('flexZeroPercent', flexZeroPercentFilter)
        .filter('flexNAPercent', flexNAPercentFilter)
        .filter('orderObjectBy', orderObjectBy);

    function asDateFilter() {
        return function (input) {
            return new Date(input);
        };
    }

    function booleanYNFilter() {
        return function(input) {
            if(!_.isBoolean(input)) { return input; }
            if(input) {
                return 'Y';
            } else {
                return 'N';
            }
        };
    }

    function booleanYesNoFilter() {
        return function(input) {
            if(!_.isBoolean(input)) { return input; }
            if(input) {
                return 'Yes';
            } else {
                return 'No';
            }
        };
    }

    function dateFmtFilter($filter) {
        return function (input) {
            if (input === null) {
                return '';
            }

            return moment(input).format('MM/DD/YYYY h:mm A');
        };
    }

    function capitalizeFilter () {
        return function (input, format) {
            if (!input) {
                return input;
            }
            format = format || 'all';
            if (format === 'first') {
                // Capitalize the first letter of a sentence
                return input.charAt(0).toUpperCase() + input.slice(1).toLowerCase();
            } else {
                var words = input.split(' ');
                var result = [];
                words.forEach(function(word) {
                    if (word.length === 2 && format === 'team') {
                        // Uppercase team abbreviations like FC, CD, SD
                        result.push(word.toUpperCase());
                    } else {
                        result.push(word.charAt(0).toUpperCase() + word.slice(1).toLowerCase());
                    }
                });
                return result.join(' ');
            }
        };
    }

    function capitalizeFirstFilter() {
        return function (input) {
            return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1);
            }) : '';
        };
    }

    function displaynameFilter() {
        return function (input) {
            if (!input) {
                return '';
            }
            if (typeof input !== 'string')
            {
                return '';
            }
            if (input.indexOf(',') < 1) {
                return input;
            }
            return input.substring(input.indexOf(',') + 1) + ' ' + input.substring(0, input.indexOf(','));
        };
    }

    function displayNAFilter() {
        return function (input) {
            if (!input) {
                return 'N/A';
            }
            if (parseFloat(input) !== 0) {
                return input;
            }
            return ' - ';
        };
    }

    function displaynullFilter() {
        return function (input) {
            if (!input) {
                return ' - ';
            }
            if (parseFloat(input) !== 0) {
                return input;
            }
            return ' - ';
        };
    }

    function displaynullcurrencyFilter($filter) {
        return function (input) {
            if (!input) {
                return ' - ';
            }
            if (parseFloat(input) !== 0) {
                return $filter('currency')(input);
            }
            return ' - ';
        };
    }

    function displaynullpercentFilter($filter) {
        return function (input) {
            if (!input) {
                return ' - ';
            }
            if (parseFloat(input) !== 0) {
                return $filter('number')(input, 1) + '%';
            }
            return ' - ';
        };
    }

    function displaynullsingleFilter($filter) {
        return function (input) {
            if (!input) {
                return ' - ';
            }
            if (parseFloat(input) !== 0) {
                return $filter('number')(input, 1);
            }
            return ' - ';
        };
    }

    function displaypercentFilter($filter) {
        return function (input) {
            return $filter('number')(input, 0) + '%';
        };
    }

    function booleanFilter($filter) {
        return function (input) {
            if (input === 1 || input === true) {
                return 'Yes';
            }
            return 'No';
        };
    }

    function noCentsCurrencyFilter($filter, $locale) {
        var currencyFilter = $filter('currency');
        var formats = $locale.NUMBER_FORMATS;
        return function (amount, currencySymbol) {
            if (!amount) {
                return ' - ';
            }
            var value = currencyFilter(amount, currencySymbol);
            var sep = value.indexOf(formats.DECIMAL_SEP);
            //console.log(amount, value);
            if (amount >= 0) {
                return value.substring(0, sep);
            }
            return value.substring(0, sep) + ')';
        };

    }

    function phoneFilter() {
        return function (input) {
            if (!input) {
                return '';
            }
            if (input.length < 10) {
                return input;
            }
            return '(' + input.substr(0, 3) + ') ' + input.substr(3, 3) + '-' + input.substr(6, 4);
        };
    }

    function ssnumFilter() {
        return function (input) {
            if (!input) { return ''; }

            if (input.length !== 9) {
                return input;
            }
            var ssn = input.substr(0, 3) + '-' + input.substr(3, 2) + '-' + input.substr(5, 4);
            return ssn;
        };
    }

    function justtextFilter(input) {
        return function (input) {
            return input;
        };
    }

    function flexNumberFilter($filter) {
        return function (input, decPlaces) {
            decPlaces = decPlaces || 0;

            // Check for invalid inputs
            if (isNaN(input)) {
                return input;
            }
            if (input === '' || input === null || input === 0 || input === -0) {
                return ' - ';
            }
            var out = input;

            //Deal with the minus (negative numbers)
            var minus = input < 0;
            out = Math.abs(out);
            out = $filter('number')(out, decPlaces);

            // Add the minus and the symbol
            if (minus) {
                return '( ' + out + ')';
            } else {
                return out;
            }
        };
    }

    function flexZeroNumberFilter($filter) {
        return function (input, decPlaces) {
            decPlaces = decPlaces || 0;

            // Check for invalid inputs
            if (isNaN(input)) {
                return input;
            }
            if (input === '' || input === null) {
                return ' - ';
            }
            var out = input;

            //Deal with the minus (negative numbers)
            var minus = input < 0;
            out = Math.abs(out);
            out = $filter('number')(out, decPlaces);

            // Add the minus and the symbol
            if (minus) {
                return '( ' + out + ')';
            } else {
                return out;
            }
        };
    }

    function flexNACurrencyFilter($filter) {
        return function (input, decPlaces) {
            decPlaces = decPlaces || 0;

            // Check for invalid inputs
            if (isNaN(input)) {
                return input;
            }
            if (input === '' || input === null) {
                return 'N/A';
            } else if(input === 0 || input === -0) {
                return ' - ';
            }
            var out = input;

            //Deal with the minus (negative numbers)
            var minus = input < 0;
            out = Math.abs(out);
            out = $filter('number')(out, decPlaces);

            // Add the minus and the symbol
            if (minus) {
                return '( $' + out + ')';
            } else {
                return '$' + out;
            }
        };
    }

    function flexCurrencyFilter($filter) {
        return function (input, decPlaces) {
            decPlaces = decPlaces || 0;

            // Check for invalid inputs
            if (isNaN(input) || !input || Math.abs(input) === 0 || (Math.abs(input) > 0 && Math.abs(input) < 1)) {
                return '-';
            }
            var out = input;

            //Deal with the minus (negative numbers)
            var minus = out < 0;
            out = Math.abs(out);
            out = $filter('number')(out, decPlaces);

            // Add the minus and the symbol
            if (minus) {
                return '( $' + out + ')';
            } else {
                return '$' + out;
            }
        };
    }

    function flexZeroCurrencyFilter($filter) {
        return function (input, decPlaces) {
            decPlaces = decPlaces || 0;

            // Check for invalid inputs
            if (isNaN(input)) {
                return ' - ';
            }
            var out = input;

            //Deal with the minus (negative numbers)
            var minus = out < 0;
            out = Math.abs(out);
            out = $filter('number')(out, decPlaces);

            // Add the minus and the symbol
            if (minus) {
                return '( $' + out + ')';
            } else {
                return '$' + out;
            }
        };
    }

    function flexNAPercentFilter($filter) {
        return function (input, decPlaces) {
            decPlaces = decPlaces || 0;

            // Check for invalid inputs
            if (isNaN(input)) {
                return input;
            }
            if (input === '' || input === null) {
                return 'N/A';
            }
            var out = input;

            //Deal with the minus (negative numbers)
            var minus = input < 0;
            out = Math.abs(out);
            out = $filter('number')(out, decPlaces);

            // Add the minus and the symbol
            if (minus) {
                return '( ' + out + '%)';
            } else {
                return out + '%';
            }
        };
    }

    function stateAbrFilter() {
        var statesHash = {
            1: 'AL',
            2: 'AK',
            3: 'AZ',
            4: 'AR',
            5: 'CA',
            6: 'CO',
            7: 'CT',
            8: 'DE',
            9: 'DC',
            10: 'FL',
            11: 'GA',
            12: 'HI',
            13: 'ID',
            14: 'IL',
            15: 'IN',
            16: 'IA',
            17: 'KS',
            18: 'KY',
            19: 'LA',
            20: 'ME',
            21: 'MD',
            22: 'MA',
            23: 'MI',
            24: 'MN',
            25: 'MS',
            26: 'MO',
            27: 'MT',
            28: 'NE',
            29: 'NV',
            30: 'NH',
            31: 'NJ',
            32: 'NM',
            33: 'NY',
            34: 'NC',
            35: 'ND',
            36: 'OH',
            37: 'OK',
            38: 'OR',
            39: 'PA',
            40: 'RI',
            41: 'SC',
            42: 'SD',
            43: 'TN',
            44: 'TX',
            45: 'UT',
            46: 'VT',
            47: 'VA',
            48: 'WA',
            49: 'WV',
            50: 'WI',
            51: 'WY',
            52: 'USA'
        };

        return function(input) {
            if (!input) {
                return '';
            } else {
                return statesHash[input];
            }
        };
    }

    function flexPercentFilter($filter) {
        return function (input, decPlaces) {
            decPlaces = decPlaces || 0;

            // Check for invalid inputs
            if (isNaN(input)) {
                return input;
            }
            if (input === '' || input === null || input === 0 || input === -0) {
                return ' - ';
            }
            var out = input;

            //Deal with the minus (negative numbers)
            var minus = input < 0;
            out = Math.abs(out);
            out = $filter('number')(out, decPlaces);

            // Add the minus and the symbol
            if (minus) {
                return '( ' + out + '%)';
            } else {
                return out + '%';
            }
        };
    }

    function flexZeroPercentFilter($filter) {
        return function (input, decPlaces) {
            decPlaces = decPlaces || 0;

            // Check for invalid inputs
            if (isNaN(input)) {
                return input;
            }
            if (input === '' || input === null) {
                return ' - ';
            }
            var out = input;

            //Deal with the minus (negative numbers)
            var minus = input < 0;
            out = Math.abs(out);
            out = $filter('number')(out, decPlaces);

            // Add the minus and the symbol
            if (minus) {
                return '( ' + out + '%)';
            } else {
                return out + '%';
            }
        };
    }

    function orderObjectBy() {
        return function (items, field, reverse) {
            var filtered = [];
            angular.forEach(items, function (item) {
                filtered.push(item);
            });
            filtered.sort(function (a, b) {
                return (a[field] > b[field] ? 1 : -1);
            });
            if (reverse) {
                filtered.reverse();
            }

            return filtered;
        };
    }
}());

(function() {
    'use strict';
    angular
        .module('ECO')
        .config(function($stateProvider, $urlRouterProvider, $authProvider, API_URL) {
            $urlRouterProvider.otherwise('/auth');
            $stateProvider
                .state('auth', {
                    url: '/auth',
                    templateUrl: './app/user/auth.view.html',
                    controller: 'AuthController as auth'
                })
                .state('forgotpw', {
                    url: '/forgotpw',
                    templateUrl: './app/user/forgotpw.view.html',
                    controller: 'RemindController as forgot'
                })
                .state('register', {
                    url: '/register',
                    templateUrl: 'app/user/register.view.html',
                    controller: 'RegisterController as reg'
                })
                .state('register2', {
                    url: '/register2',
                    templateUrl: 'app/user/register2.view.html',
                    controller: 'RegisterController as reg'
                })
                .state('eco', {
                    url: '/eco',
                    abstract: true,
                    templateUrl: 'app/layouts/logged.view.html',
                    controller: 'UserController as user'
                })
                .state('eco.home', {
                    url: '/home',
                    templateUrl: 'app/projects/dash.tmpl.html',
                    controller: 'DashboardController as dash'
                })
                .state('eco.prefs', {
                    url: '/prefs',
                    templateUrl: 'app/user/prefs.tmpl.html',
                    controller: 'SettingsController as prefs'
                })
                .state('eco.users', {
                    url: '/users',
                    templateUrl: './app/user/users.view.html',
                    controller: 'UserController as user'
                })
                .state('eco.project', {
                    url: '/project',
                    abstract: true,
                    templateUrl: 'app/projects/new.view.html',
                    controller: 'ProjectsController as vm'
                })
                .state('eco.project.new', {
                    url: '/new',
                    templateUrl: 'app/projects/new.tmpl.html'
                })
                .state('eco.project.aircraft', {
                    url: '/aircraft',
                    templateUrl: 'app/projects/aircraft.tmpl.html'
                })
                .state('eco.project.engines', {
                    url: '/engines',
                    templateUrl: 'app/projects/engines.tmpl.html'
                })
                .state('eco.project.parts', {
                    url: '/parts',
                    templateUrl: 'app/projects/parts.tmpl.html'
                })
                .state('eco.project.summary', {
                    url: '/summary',
                    templateUrl: 'app/projects/summary.tmpl.html'
                });
        });
})();

(function() {
    'use strict';
    angular
        .module('ECO')
        .controller('MainController', MainController);

        MainController.$inject = ['$rootScope', 'AppFactory'];

        /* @ngInject */
        function MainController($rootScope, AppFactory) {
            /* jshint validthis: true */
            var vm = this;

            //BUILD ONLY
            var user = {
                firstname: 'Jonathan',
                lastname: 'Gravois',
                email: 'jongravois@gmail.com'
            };
            if( ! $rootScope.currentUser) {
                $rootScope.currentUser = user;
            }
            //BUILD ONLY

            //vm.authenticated = $rootScope.authenticated;
            vm.current_year = new Date().getFullYear();
            vm.AppFactory = AppFactory;

            vm.status = {
                isopen: false
            };

            vm.toggled = function(open) {};

            vm.toggleDropdown = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                vm.status.isopen = !vm.status.isopen;
            };
            //////////

        } // end function
})();

(function() {
    'use strict';
    angular
        .module('ECO')
        .controller('DashboardController', DashboardController);

        DashboardController.$inject = ['$state'];

        /* @ngInject */
        function DashboardController($state) {
            /* jshint validthis: true */
            var vm = this;
            vm.path = $state.current.name;
            //////////

        } // end controller
})();
(function() {
    'use strict';
    angular
        .module('ECO')
        .controller('ProjectsController', ProjectsController);

        ProjectsController.$inject = ['$state', 'FeederFactory'];

        /* @ngInject */
        function ProjectsController($state, FeederFactory) {
            /* jshint validthis: true */
            var vm = this;
            vm.pbar_start = false;

            FeederFactory
                .getMocks()
                .then(function(rsp) {
                    vm.mocks = rsp.data;
                });
            FeederFactory
                .getLocations()
                .then(function(rsp) {
                    var raw = rsp.data;
                    vm.locations = raw;
                    //console.log('Locations', raw);

                    //TODO: Remove Corporate from above
                    //vm.techlocations = techs;

                    vm.techlocations = [
                        {
                            id: 2,
                            location: "Mobile Disassembly"
                        },
                        {
                            id: 3,
                            location: 'Tupelo, Mississippi, USA'
                        }
                    ];
                });

            vm.new_pbar = {
                aircraft: false,
                engines: false,
                parts: false,
                summary: false
            };

            vm.moveToAircraft = function() {
                vm.new_pbar.aircraft = true;
                vm.pbar_start = true;
                $state.go('eco.project.aircraft', {});
            };
            vm.moveToEngines = function() {
                vm.new_pbar.engines = true;
                $state.go('eco.project.engines', {});
            };
            vm.moveToParts = function() {
                vm.new_pbar.parts = true;
                $state.go('eco.project.parts', {});
            };
            vm.moveToSummary = function() {
                vm.new_pbar.summary = true;
                $state.go('eco.project.summary', {});
            };
            vm.submitNewProject = function() {
                alert('Submitting Project for Processing and Approval');
            };
            //////////
            vm.newb = {
                project_type: "1",
            };

        } // end controller
})();
(function() {
    'use strict';
    angular
        .module('ECO')
        .factory('AppFactory', AppFactory);

    AppFactory.$inject = ['$rootScope', '$auth', '$http', '$state', 'API_URL'];

    /* @ngInject */
    function AppFactory($rootScope, $auth, $http, $state, API_URL) {
        var publicAPI = {
            logout: logout
        };
        return publicAPI;

        //////////
        function logout() {
            $auth.logout()
                .then(function() {
                    localStorage.removeItem('user');
                    $rootScope.authenticated = false;
                    $rootScope.currentUser = null;
                    $state.go('auth', {});
                });
        }
    } // end factory
})();
(function() {
    'use strict';
    angular
        .module('ECO')
        .factory('FeederFactory', FeederFactory);

    FeederFactory.$inject = ['$http', 'API_URL'];

    /* @ngInject */
    function FeederFactory($http, API_URL) {
        var publicAPI = {
            getCompanies: getCompanies,
            getCountries: getCountries,
            getLocations: getLocations,
            getMocks: getMocks
        };
        return publicAPI;

        //////////
        function getCompanies() {
            return $http.get(API_URL + 'companies');
        }
        function getCountries() {
            return $http.get(API_URL + 'countries');
        }
        function getLocations() {
            return $http.get(API_URL + 'locations');
        }
        function getMocks() {
            return $http.get(API_URL + 'mocks');
        }
    } // end factory
})();
(function() {
    'use strict';
    angular
        .module('ECO')
        .controller('AuthController', AuthController);

        AuthController.$inject = ['$rootScope', '$auth', '$state', '$http', 'API_URL'];

        /* @ngInject */
        function AuthController($rootScope, $auth, $state, $http, API_URL) {
            /* jshint validthis: true */
            var vm = this;

            vm.loginError = false;
            vm.loginErrorText;

            vm.login = function() {
                var credentials = {
                    email: vm.email,
                    password: vm.password
                };

                $auth
                    .login(credentials)
                    .then(function() {
                        return $http.get(API_URL + 'authenticate/user');
                    }, function(error) {
                        vm.loginError = true;
                        vm.loginErrorText = error.data.error;
                    })
                    .then(function(response) {
                        var resp = response.data.user;
                        var user = JSON.stringify(resp);
                        localStorage.setItem('user', user);
                        $rootScope.authenticated = true;
                        $rootScope.currentUser = resp;
                        $state.go('eco.home', {});
                    });
            };
        } // end function
})();

(function() {
    'use strict';
    angular
        .module('ECO')
        .controller('RegisterController', RegisterController);

        RegisterController.$inject = ['$state', 'FeederFactory'];

        /* @ngInject */
        function RegisterController($state, FeederFactory) {
            /* jshint validthis: true */
            var vm = this;

            FeederFactory
                .getCountries()
                .then(function(rsp) {
                    vm.countries = rsp.data;
                });

            vm.newuser = {
                individual: false
            };

            vm.companyusers = [
                {
                    firstname: 'Tony',
                    lastname: 'Stark',
                    title: 'CEO',
                    email: 'ironman@marvel.com'
                },
                {
                    firstname: 'Bruce',
                    lastname: 'Wayne',
                    title: 'CFO',
                    email: 'batman@marvel.com'
                },
                {
                    firstname: 'Natasha',
                    lastname: 'Romanoff',
                    title: 'COO',
                    email: 'blackwidow@marvel.com'
                }
            ];

            vm.addUser = function() {
                vm.companyusers.push(vm.newb);
                vm.newb = {};
            };

            vm.registerCompany = function() {
                $state.go('register2', {});
            };

            vm.registerUsers = function() {
                alert('Form Saved');
            };

            //////////

        } // end controller
})();
(function() {
    'use strict';
    angular
        .module('ECO')
        .controller('RemindController', RemindController);

        RemindController.$inject = [];

        /* @ngInject */
        function RemindController() {
            /* jshint validthis: true */
            var vm = this;

            //////////

        } // end controller
})();
(function () {
    'use strict';
    angular
        .module('ECO')
        .controller('UserController', UserController);

    UserController.$inject = ['$rootScope', '$auth', '$http', '$state', 'AppFactory', 'API_URL'];

    /* @ngInject */
    function UserController($rootScope, $auth, $http, $state, AppFactory, API_URL) {
        /* jshint validthis: true */
        var vm = this;
        vm.users = [];
        vm.error = [];

        vm.getUsers = function () {
            $http.get(API_URL + 'authenticate')
                .success(function (users) {
                    vm.users = users;
                })
                .error(function (error) {
                    vm.error = error;
                });
        };
        vm.logout = AppFactory.logout;
    } // end controller
})();

(function() {
    'use strict';
    angular
        .module('ECO')
        .directive('helpdot', function() {
            return {
                scope: {
                    msg: '@'
                },
                restrict: 'E',
                templateUrl: './_modules/helpdot/helpdot.tmpl.html'
            };
        });
})();
(function() {
    'use strict';
    angular
        .module('ECO')
        .controller('SidebarController', SidebarController);

        SidebarController.$inject = [];

        /* @ngInject */
        function SidebarController() {
            /* jshint validthis: true */
            var vm = this;
            //////////
            vm.oneAtATime = true;
            vm.status = {
                isFirstOpen: false,
                isFirstDisabled: false
            };

        } // end controller
})();
(function() {
    'use strict';
    angular
            .module('ECO')
            .directive('userbar', function() {
                return {
                    transclude: true,
                    templateUrl: './_modules/userbar/userbar.tmpl.html'
                };
            });
})();
//# sourceMappingURL=app.js.map
