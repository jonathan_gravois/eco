var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var minify = require('gulp-minify-css');
var notify = require('gulp-notify');
var util = require('gulp-util');
var del = require('del');

gulp.task('styles', ['clean-styles'], function() {
    log('Cleaning out old CSS');
    clean('./css/styles.css');
    log('Compiling SASS --> CSS');
    return gulp
        .src('./sass/styles.scss')
        .pipe(sass())
        .pipe(concat('styles.css'))
        .pipe(minify())
        .pipe(gulp.dest('./css'))
        .pipe(notify({ message: 'Styles task complete' }));
});

//////////
function clean(path, done) {
    log('Cleaning ' + util.colors.blue(path));
    del(path, done);
}
function log(msg) {
    if (typeof(msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                util.log(util.colors.blue(msg[item]));
            }
        }
    } else {
        util.log(util.colors.blue(msg));
    }
}