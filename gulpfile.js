var gulp = require('gulp');
var args = require('yargs').argv;
var config = require('./gulp.config')();
var del = require('del');
var iff = require('gulp-if');
var jscs = require('gulp-jscs');
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify-css');
var notify = require('gulp-notify');
var util = require('gulp-util');
var shell = require('gulp-shell');
var plumber = require('gulp-plumber');
var taskListing = require('gulp-task-listing');
var runSequence = require('run-sequence');
var livereload = require('gulp-livereload');

var jasmine = require('gulp-jasmine');
///////////
gulp.task('help', taskListing);
gulp.task('speak', function(message){
    return gulp
        .src('')
        .pipe(shell('say ' + message));
});
// STYLESHEETS
gulp.task('styles', function (cb) {
    runSequence(['vendor-styles', 'app-styles'], cb);
});
gulp.task('app-styles', function() {
    clean(config.css + 'styles.css');
    log('Removed old CSS');
    log('Compiling SASS --> CSS');
    return gulp
        .src('./sass/styles.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .on('error', errorLogger)
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./css'))
        .pipe(notify({message: 'Styles task complete'}));
});
gulp.task('vendor-styles', function() {
    clean(config.css + 'vendor.css');
    log('Removed old Vendor CSS');
    log('Compiling Vendor CSS');
    return gulp
        .src([
            './js/angular-toastr/angular-toastr.css',
            './js/angular-loading-bar/loading-bar.css'
        ])
        .pipe(sourcemaps.init())
        .pipe(concat('vendor.css'))
        .pipe(minify())
        .on('error', errorLogger)
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./css'))
        .pipe(notify({message: 'Vendor CSS task complete'}));
});
// JAVASCRIPT
gulp.task('vet', function() {
    log('Analyzing source with JSHint and JSCS');

    return gulp
        .src(config.alljs)
        .pipe(jscs())
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish', {verbose: true}))
        .pipe(jshint.reporter('fail'));
});
gulp.task('scripts', function (cb) {
    runSequence(['vendor-scripts', 'app-scripts'], cb);
});
gulp.task('app-scripts', function() {
    clean('./scripts/app.js');
    log('Removed old Application scripts');
    log('Processing Application scripts');
    return gulp
        .src(config.appscripts)
        .pipe(notify({message: 'Grabbing all application scripts'}))
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        .on('error', errorLogger)
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./scripts/'))
        .pipe(notify({message: 'Application scripts task complete'}));
});
gulp.task('vendor-scripts', function() {
    clean('./scripts/vendor.js');
    log('Removed old Vendor scripts');
    log('Processing Vendor scripts');
    return gulp
        .src(config.vendorscripts)
        .pipe(notify({message: 'Grabbing all vendor scripts'}))
        .pipe(sourcemaps.init())
        .pipe(concat('vendor.js'))
        .on('error', errorLogger)
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./scripts/'))
        .pipe(notify({message: 'Vendor scripts task complete'}));
});
// KARMA / JASMINE TESTING
gulp.task('specs', function () {
    return gulp.src('./specs/**.js')
        .pipe(jasmine())
        .pipe(notify({message: 'Jasmine complete'}));
});
// WATCHERS
gulp.task('watch', function() {
    livereload.listen();
    gulp.watch([config.allsass], ['app-styles']);
    gulp.watch([config.alljs], ['vendor-scripts', 'app-scripts'])
    gulp.watch(['specs/**.js', config.alljs], ['specs']);
});
// DEFAULT
gulp.task('default', ['styles', 'scripts', 'specs', 'watch']);
///////////
function clean(path, done) {
    log('Cleaning ' + util.colors.blue(path));
    del(path, done);
}
function errorLogger(error) {
    log('*** Error Start ***');
    log(error);
    log('*** Error End ***');
    this.emit('end');
}
function log(msg) {
    if (typeof(msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                util.log(util.colors.blue(msg[item]));
            }
        }
    } else {
        util.log(util.colors.blue(msg));
    }
}
