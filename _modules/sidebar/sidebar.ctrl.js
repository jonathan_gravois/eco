(function() {
    'use strict';
    angular
        .module('ECO')
        .controller('SidebarController', SidebarController);

        SidebarController.$inject = [];

        /* @ngInject */
        function SidebarController() {
            /* jshint validthis: true */
            var vm = this;
            //////////
            vm.oneAtATime = true;
            vm.status = {
                isFirstOpen: false,
                isFirstDisabled: false
            };

        } // end controller
})();