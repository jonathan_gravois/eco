(function() {
    'use strict';
    angular
            .module('ECO')
            .directive('userbar', function() {
                return {
                    transclude: true,
                    templateUrl: './_modules/userbar/userbar.tmpl.html'
                };
            });
})();