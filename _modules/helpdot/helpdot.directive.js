(function() {
    'use strict';
    angular
        .module('ECO')
        .directive('helpdot', function() {
            return {
                scope: {
                    msg: '@'
                },
                restrict: 'E',
                templateUrl: './_modules/helpdot/helpdot.tmpl.html'
            };
        });
})();